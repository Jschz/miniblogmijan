package de.awacademy.miniblogmijan.kommentar;

import de.awacademy.miniblogmijan.beitrag.Beitrag;
import de.awacademy.miniblogmijan.beitrag.BeitragService;
import de.awacademy.miniblogmijan.nutzer.Nutzer;
import de.awacademy.miniblogmijan.nutzer.NutzerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.Instant;

@Controller
public class KommentarController {
    private BeitragService beitragService;
    private NutzerService nutzerService;
    private KommentarService kommentarService;
    private Beitrag aktuellerBeitrag;

    @Autowired
    public KommentarController ( BeitragService beitragService, NutzerService nutzerService, KommentarService kommentarService ) {
        this.beitragService = beitragService;
        this.nutzerService = nutzerService;
        this.kommentarService = kommentarService;
    }

    @PostMapping("beitrag/{id}/addedcomment")
    public String addComment( @ModelAttribute("sessionUser") Nutzer sessionUser, @ModelAttribute("kommentar") KommentarDTO kommentar, @PathVariable int id){
        aktuellerBeitrag = beitragService.getBeitragPerId (id);
        Kommentar newComm = new Kommentar(sessionUser,kommentar.getText(), Instant.now());

        newComm.setBeitrag(aktuellerBeitrag);
        if(nutzerService.findNutzerById(sessionUser.getId()).isPresent()){
            newComm.setNutzerKommentar(sessionUser);
            kommentarService.putCommentInRepo(newComm);
        }
        return "redirect:/beitrag/{id}";
    }

    @PostMapping("/beitrag/{id}/commentdeleted")
    public String deleteComment(@ModelAttribute("sessionUser") Nutzer sessionUser, @PathVariable int id, @RequestParam("commentId") int commentId){
        kommentarService.deleteCommentInRepo(commentId);
        return "redirect:/beitrag/{id}";
    }
}
