package de.awacademy.miniblogmijan.kommentar;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface KommentarRepository extends CrudRepository <Kommentar, Long> {
    List <Kommentar> findAllByOrderByPostedAtAsc ();

}
