package de.awacademy.miniblogmijan.kommentar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class KommentarService {

    private KommentarRepository kommentarRepository;

    @Autowired
    public KommentarService(KommentarRepository kommentarRepository) {
        this.kommentarRepository = kommentarRepository;
    }

    public List<Kommentar> getKommentar() {
        return kommentarRepository.findAllByOrderByPostedAtAsc();
    }

    public void putCommentInRepo(Kommentar kommentar){
        kommentarRepository.save(kommentar);
    }

    public void deleteCommentInRepo(long id){
        kommentarRepository.deleteById(id);
    }
}
