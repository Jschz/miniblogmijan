package de.awacademy.miniblogmijan.kommentar;

public class KommentarDTO {
    String text;

    public KommentarDTO(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
