package de.awacademy.miniblogmijan.kommentar;

import de.awacademy.miniblogmijan.beitrag.Beitrag;
import de.awacademy.miniblogmijan.nutzer.Nutzer;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.time.Instant;

@Entity
public class Kommentar {

    @Id
    @GeneratedValue
    private long id;

    @ManyToOne
    private Nutzer nutzerKommentar;

    @ManyToOne
    private Beitrag beitrag;

    private String text;
    private Instant postedAt;

    public Kommentar() {
    }

    public Kommentar (Nutzer nutzer, String text, Instant postedAt ) {
        this.nutzerKommentar = nutzer;
        this.text = text;
        this.postedAt = postedAt;
    }

    public long getId () {
        return id;
    }

    public Nutzer getNutzer () {
        return nutzerKommentar;
    }

    public String getText () {
        return text;
    }

    public Instant getPostedAt () {
        return postedAt;
    }

    public Nutzer getNutzerKommentar () {
        return nutzerKommentar;
    }

    public Beitrag getBeitrag () {
        return beitrag;
    }

    public void setNutzerKommentar(Nutzer nutzerKommentar) {
        this.nutzerKommentar = nutzerKommentar;
    }

    public void setBeitrag(Beitrag beitrag) {
        this.beitrag = beitrag;
    }
}
