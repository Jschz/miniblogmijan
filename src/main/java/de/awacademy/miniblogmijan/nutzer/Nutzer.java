package de.awacademy.miniblogmijan.nutzer;

import de.awacademy.miniblogmijan.beitrag.Beitrag;
import de.awacademy.miniblogmijan.kommentar.Kommentar;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
public class Nutzer {

    @Id
    @GeneratedValue
    private long id;

    private String username;
    private String password;
    private boolean isAdmin = false;

    @OneToMany(mappedBy = "nutzerBeitrag")
    private List<Beitrag> beitraege;

    @OneToMany(mappedBy = "nutzerKommentar")
    private List<Kommentar> kommentare;

    public Nutzer() {
    }

    public Nutzer(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public List<Beitrag> getMessages() {
        return beitraege;
    }

    public long getId () {
        return id;
    }

    public List<Beitrag> getBeitraege () {
        return beitraege;
    }

    public List<Kommentar> getKommentare () {
        return kommentare;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        this.isAdmin = admin;
    }

}
