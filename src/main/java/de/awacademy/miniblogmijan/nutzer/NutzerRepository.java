package de.awacademy.miniblogmijan.nutzer;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface NutzerRepository extends CrudRepository <Nutzer, Long> {
    Optional<Nutzer> findByUsernameAndPassword ( String username, String password);
    boolean existsByUsername (String username);
    Optional<Nutzer>findByUsername (String username);
    Optional<Nutzer>findById(long id);
}
