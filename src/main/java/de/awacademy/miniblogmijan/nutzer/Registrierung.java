package de.awacademy.miniblogmijan.nutzer;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class Registrierung {

    @NotEmpty
    @Pattern(regexp = "^[a-zA-Z0-9]+$", message = "Darf nur Buchstaben und Zahlen enthalten.")
    private String username;

    @Size(min = 5, max = 10, message = "Das Passwort muss 5-10 Zeichen enthalten.")
    private String password1;
    private String password2;

    public Registrierung ( String username, String password1, String password2) {
        this.username = username;
        this.password1 = password1;
        this.password2 = password2;

    }

    public String getUsername() {
        return username;
    }

    public String getPassword1() {
        return password1;
    }

    public String getPassword2() {
        return password2;
    }
}
