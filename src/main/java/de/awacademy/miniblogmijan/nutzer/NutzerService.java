package de.awacademy.miniblogmijan.nutzer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class NutzerService {

    private NutzerRepository nutzerRepository;

    @Autowired
    public NutzerService ( NutzerRepository nutzerRepository ) {
        this.nutzerRepository = nutzerRepository;
    }

    public boolean uniqueUser (String username){
        return nutzerRepository.existsByUsername(username);
    }

    public void save (Nutzer nutzer){
        nutzerRepository.save(nutzer);
    }

    public Optional<Nutzer> findUser ( String username, String password){
        return nutzerRepository.findByUsernameAndPassword(username,password);
    }

    public Optional<Nutzer> findNutzerById (long id){
        return nutzerRepository.findById(id);
    }

    public Optional<Nutzer> findNutzerByName (String name){
        return nutzerRepository.findByUsername ( name );
    }

    public void putUserinRepo (Nutzer user){
        nutzerRepository.save(user);
    }
}


