package de.awacademy.miniblogmijan.nutzer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class NutzerController {

    private NutzerService nutzerService;

    @Autowired
    public NutzerController ( NutzerService nutzerService ) {
        this.nutzerService = nutzerService;
    }

    @GetMapping("/register")
    public String register( Model model) {
        model.addAttribute("registrierung", new Registrierung ("", "", ""));
        return "registrierung";
    }

    @PostMapping("/register")
    public String register( @Valid @ModelAttribute("registrierung") Registrierung registrierung, BindingResult bindingResult) {

        if(!registrierung.getPassword1().equals(registrierung.getPassword2())) {
            bindingResult.addError(new FieldError ("registrierung", "password2", "Mach da das gleiche rein"));
        }

        if(nutzerService.uniqueUser (registrierung.getUsername())) {
            bindingResult.addError(new FieldError("registrierung", "username", "Den jibbet scho"));
        }

        if(bindingResult.hasErrors()) {
            return "registrierung";
        }

        Nutzer nutzer = new Nutzer (registrierung.getUsername(), registrierung.getPassword1());
        nutzerService.save(nutzer);

        return "redirect:/login";
    }


}
