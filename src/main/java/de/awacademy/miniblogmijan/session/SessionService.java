package de.awacademy.miniblogmijan.session;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Optional;

@Service
public class SessionService {

    private SessionRepository sessionRepository;

    @Autowired
    public SessionService ( SessionRepository sessionRepository ) {
        this.sessionRepository = sessionRepository;
    }

    public void save ( Session session){
        sessionRepository.save(session);
    }

    public Optional<Session> isValid ( String id, Instant time ){
        return sessionRepository.findByIdAndExpiresAtAfter(id, time);
    }

    public Optional<Session> findByIdAndExpiresAfter (String id, Instant expiresAt){
        return sessionRepository.findByIdAndExpiresAtAfter ( id, expiresAt );
    }

    public void delete ( Session session){
        sessionRepository.delete(session);
    }
}
