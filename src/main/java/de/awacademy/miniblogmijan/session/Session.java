package de.awacademy.miniblogmijan.session;

import de.awacademy.miniblogmijan.nutzer.Nutzer;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.time.Instant;
import java.util.UUID;

@Entity
public class Session {

    @Id
    private String id = UUID.randomUUID().toString();

    @ManyToOne
    private Nutzer nutzer;

    private Instant expiresAt;

    public Session () {
    }

    public Session ( Nutzer nutzer, Instant expiresAt) {
        this.nutzer = nutzer;
        this.expiresAt = expiresAt;
    }

    public Nutzer getNutzer() {
        return nutzer;
    }

    public String getId() {
        return id;
    }

    public void setExpiresAt(Instant expiresAt) {
        this.expiresAt = expiresAt;
    }
}
