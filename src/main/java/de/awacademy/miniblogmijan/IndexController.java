package de.awacademy.miniblogmijan;

import de.awacademy.miniblogmijan.beitrag.BeitragDTO;
import de.awacademy.miniblogmijan.beitrag.BeitragService;
import de.awacademy.miniblogmijan.nutzer.Nutzer;
import de.awacademy.miniblogmijan.nutzer.NutzerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class IndexController {

    private BeitragService beitragService;
    private NutzerService nutzerService;
    private int upgradeUserId;

    @Autowired
    public IndexController(BeitragService beitragService, NutzerService nutzerService) {
        this.beitragService = beitragService;
        this.nutzerService = nutzerService;
    }

    @GetMapping("/")
    public String home(@ModelAttribute("sessionUser") Nutzer sessionUser, Model model) {
        model.addAttribute("beitraege", beitragService.getBeitrag ());
        model.addAttribute("beitrag", new BeitragDTO(""));
        return "index";
    }

    @PostMapping("/makeAdmin")
    public String makeAdmin(Model model,@RequestParam("makeUserIdToAdmin") String name, @ModelAttribute("sessionUser") Nutzer sessionUser){

        if(nutzerService.findNutzerByName (name).isPresent()){
            nutzerService.findNutzerByName (name).get().setAdmin(true);
            Nutzer newNutzer = nutzerService.findNutzerByName (name).get();
            nutzerService.putUserinRepo(newNutzer);
        }
        return home(sessionUser,model);
    }

    public int getUpgradeUserId() {
        return upgradeUserId;
    }

    public void setUpgradeUserId(int upgradeUserId) {
        this.upgradeUserId = upgradeUserId;
    }
}
