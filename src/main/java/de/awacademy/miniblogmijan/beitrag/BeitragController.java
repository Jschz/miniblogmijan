package de.awacademy.miniblogmijan.beitrag;

import de.awacademy.miniblogmijan.kommentar.KommentarDTO;
import de.awacademy.miniblogmijan.nutzer.Nutzer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.Instant;

@Controller
public class BeitragController {

    private BeitragService beitragService;
    private Beitrag aktuellerBeitrag;

    @Autowired
    public BeitragController(BeitragService beitragService) {
        this.beitragService = beitragService;
    }

    @GetMapping("/beitrag/{id}")
    public String beitragsDetails( Model model, @ModelAttribute("sessionUser") Nutzer sessionUser, @PathVariable int id) {
        aktuellerBeitrag = beitragService.getBeitragPerId ( id );
        model.addAttribute("beitrag", aktuellerBeitrag);
        model.addAttribute("kommentarDTO", new KommentarDTO(""));
        return "beitrag";
    }

    @PostMapping("/")
    public String addBeitrag(Model model, @ModelAttribute("sessionUser") Nutzer sessionUser,@Valid @ModelAttribute("beitrag") BeitragDTO beitrag, BindingResult bindingResult) {
        Beitrag newBeitrag = new Beitrag(sessionUser, beitrag.getTitel(),beitrag.getText(), Instant.now());

        if(bindingResult.hasErrors()) {
            model.addAttribute ( "beitraege",beitragService.getBeitrag () );
            return "index";
        }

        beitragService.putBeitragInRepo(newBeitrag);

        return "redirect:/";
    }


}
