package de.awacademy.miniblogmijan.beitrag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BeitragService {

    private BeitragRepository beitragRepository;

    @Autowired
    public BeitragService ( BeitragRepository beitragRepository ) {
        this.beitragRepository = beitragRepository;
    }

    public List <Beitrag> getBeitrag(){
        return beitragRepository.findAllByOrderByPostedAtDesc ();
    }

    public Beitrag getBeitragPerId(int id){
        return beitragRepository.findById ( (long) id ).get ();
    }

    public void putBeitragInRepo (Beitrag beitrag){
        beitragRepository.save(beitrag);
    }
}
