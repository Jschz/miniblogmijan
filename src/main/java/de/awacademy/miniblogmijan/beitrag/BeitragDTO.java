package de.awacademy.miniblogmijan.beitrag;

import javax.validation.constraints.NotEmpty;

public class BeitragDTO {
    @NotEmpty(message = "Mach da wat rein")
    String titel;
    @NotEmpty(message = "Mach da wat rein")
    String text;

    public BeitragDTO(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTitel() {
        return titel;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }
}
