package de.awacademy.miniblogmijan.beitrag;

import de.awacademy.miniblogmijan.kommentar.Kommentar;
import de.awacademy.miniblogmijan.nutzer.Nutzer;

import javax.persistence.*;
import java.time.Instant;
import java.util.List;

@Entity
public class Beitrag {

    @Id
    @GeneratedValue
    private long id;

    @ManyToOne
    private Nutzer nutzerBeitrag;

    @OneToMany(mappedBy = "beitrag")
    private List<Kommentar> kommentare;

    private String titel;

    @Column(columnDefinition = "LONGTEXT")
    private String text;

    private Instant postedAt;

    public Beitrag () {
    }

    public Beitrag(Nutzer nutzerBeitrag, String titel, String text, Instant postedAt) {
        this.nutzerBeitrag = nutzerBeitrag;
        this.titel = titel;
        this.text = text;
        this.postedAt = postedAt;
    }

    public Nutzer getNutzer() {
        return nutzerBeitrag;
    }

    public String getText() {
        return text;
    }

    public Instant getPostedAt() {
        return postedAt;
    }

    public long getId() {
        return id;
    }

    public Nutzer getNutzerBeitrag () {
        return nutzerBeitrag;
    }

    public List<Kommentar> getKommentare () {
        return kommentare;
    }

    public String getTitel() {
        return titel;
    }
}
