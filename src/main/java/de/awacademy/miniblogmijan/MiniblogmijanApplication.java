package de.awacademy.miniblogmijan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MiniblogmijanApplication {

    public static void main ( String[] args ) {
        SpringApplication.run ( MiniblogmijanApplication.class, args );
    }

}
